/**
 * Create a stack object that can be printed on the game screen. A stack is
 * represented by a pre-determined pattern given by the Tetris rules. The
 * maximum length of a stack is given by the "I" shape pattern which is 4 cells
 * long, therefore all the Tetris patterns can be represented using a 4x4
 * matrix. The possible patterns are : the 2x2 cube, the I shape, the T shape,
 * the L shape, the inverted L shape, the Z shape and the inverted Z shape.
 */
var Stack = function (x, y) {

    /*
     * Properties
     */
    this.x = x; // X position on the grid in cell index
    this.y = y; // Y position on the grid in cell index
    this.width = 0; // Stack width on the 4x4 pattern
    this.height = 0; // Stack height on the 4x4 pattern
    this.color = "Red"; // Default color
    this.pattern = [ [ 1, 1, 1, 1 ], [ 1, 1, 1, 1 ], [ 1, 1, 1, 1 ], [ 1, 1, 1, 1 ] ];

    /*
     * Functions
     */

    /**
     * This function generates a random stack by generating a random number
     * between 1 and 7 and selects a pattern and a color for this stack
     * according to it.
     */
    this.assignRandomPattern = function () {
        // Random number between 1 and 7
        var rand = Math.floor(Math.random() * 7) + 1;

        switch (rand) {
        case 1: // Cube 2x2 pattern
            this.pattern = [ [ 1, 1, 0, 0 ], [ 1, 1, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ] ];
            this.color = "Yellow";
            break;
        case 2: // I pattern
            this.pattern = [ [ 1, 1, 1, 1 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ] ];
            this.color = "DeepSkyBlue";
            break;
        case 3: // T pattern
            this.pattern = [ [ 1, 1, 1, 0 ], [ 0, 1, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ] ];
            this.color = "BlueViolet";
            break;
        case 4: // L pattern
            this.pattern = [ [ 1, 1, 1, 0 ], [ 1, 0, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ] ];
            this.color = "DarkOrange";
            break;
        case 5: // Inverted L pattern
            this.pattern = [ [ 1, 1, 1, 0 ], [ 0, 0, 1, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ] ];
            this.color = "Blue";
            break;
        case 6: // Z pattern
            this.pattern = [ [ 1, 1, 0, 0 ], [ 0, 1, 1, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ] ];
            this.color = "Red";
            break;
        case 7: // Inverted Z pattern
            this.pattern = [ [ 0, 1, 1, 0 ], [ 1, 1, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ] ];
            this.color = 'Chartreuse';
            break;
        default:
            alert("ERROR : Invalid random number");
        }

        // Update bounds
        this.computeBounds();
    }

    /**
     * Compute the bounding box of this stack. The bounding box is the maximum
     * width and height of the stack occupied in the 4x4 matrix representing the
     * pattern. This bounding box is used to compute collisions in the game loop
     * to know whereas a stack is touching the floor or another occupied cell.
     */
    this.computeBounds = function () {
        // Reset bounds
        this.width = 0;
        this.height = 0;

        // Compute max width
        for (var line = 0; line < 4; line++) {
            for (var column = 0; column < 4; column++) {
                if (this.pattern[line][column] == 1) {
                    this.width = Math.max(this.width, column);
                }
            }
        }
        // Compute max height
        for (var column = 0; column < 4; column++) {
            for (var line = 0; line < 4; line++) {
                if (this.pattern[line][column] == 1) {
                    this.height = Math.max(this.height, line);
                }
            }
        }
        this.width++;
        this.height++;
    }
}