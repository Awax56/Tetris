/**
 * Map object consisting of a 'mapHeight' by 'mapWidth' matrix, containing the
 * falling stack moved by the player and the map cells which can be empty or
 * filled by the stacks dropped by the player.
 */
var Map = function () {
    /*
     * Properties
     */
    this.mapHeight = 10; // Height of map grid in number of cells
    this.mapWidth = 8; // Width of map grid in number of cells
    this.mapX = 10; // Delta in pixels from the left border
    this.mapY = 10; // Delta in pixels from the top border
    this.cellWidth = 40; // Cell size in pixels
    this.stroke = 3; // Stroke width in pixels
    this.map = [];
    this.fallingstack = null;

    /*
     * Functions
     */

    /**
     * Initializes the map with empty cells and create a random stack.
     */
    this.initMap = function () {
        // Fill map with white cells
        for (var line = 0; line < this.mapHeight; line++) {
            this.map.push(new Array(this.mapWidth));
            for (var column = 0; column < this.mapWidth; column++) {
                this.map[line][column] = null;
            }
        }

        // Create falling stack
        this.fallingStack = new Stack(this.mapWidth / 2 - 1, 0);
        this.fallingStack.assignRandomPattern();
    }

    /**
     * Compute game gravity by decreasing the position of the falling stack on
     * the y axis. If the stack can't fall anymore then the stack is dropped and
     * the cells are filled on the map, if the top of the map is reached and the
     * stack can't be dropped, then the game is over.
     */
    this.gravity = function () {
        // If falling stack does not touch the floor
        if (this.isNextLineAvailable(this.fallingStack)) {
            this.fallingStack.y++;
        }
        // Check if stack is dropable
        else if (this.isStackDropable(this.fallingStack)) {
            // Drop the stack
            this.dropStack(this.fallingStack);
            // Create new stack
            this.fallingStack = new Stack(this.mapWidth / 2 - 1, 0);
            this.fallingStack.assignRandomPattern();
        } else {
            alert("Game over");
        }
        this.repaint();
    }

    /**
     * Draw the map on the screen. This function starts by drawing the map grid,
     * which consists of 'mapHeight + 1' lines and 'mapWidth + 1' columns. Then
     * it draws the map cells which are stored in the 'map' array, each cell
     * represents a color (which means the cell is not empty) or a 'null' value
     * which means that the cell is empty, the default empty cell color is then
     * used.
     */
    this.drawMap = function () {
        var canvas = document.getElementById("myCanvas");
        var ctx = canvas.getContext("2d");

        ctx.strokeStyle = "Black";
        ctx.lineWidth = this.stroke;

        // Draw grid lines
        for (var line = 0; line < this.mapHeight + 1; line++) {
            ctx.beginPath();
            ctx.moveTo(this.mapX, line * (this.cellWidth + this.stroke) + this.mapY);
            ctx.lineTo(this.mapX + this.mapWidth * (this.cellWidth + this.stroke), line
                    * (this.cellWidth + this.stroke) + this.mapY);
            ctx.stroke();
        }

        // Draw grid columns
        for (var column = 0; column < this.mapWidth + 1; column++) {
            ctx.beginPath();
            ctx.moveTo(column * (this.cellWidth + this.stroke) + this.mapX, this.mapY);
            ctx.lineTo(column * (this.cellWidth + this.stroke) + this.mapX, this.mapY + this.mapHeight
                    * (this.cellWidth + this.stroke));
            ctx.stroke();
        }

        // Draw cells
        for (var line = 0; line < this.mapHeight; line++) {
            for (var column = 0; column < this.mapWidth; column++) {
                // Empty cell
                if (this.map[line][column] == null) {
                    ctx.fillStyle = "LightGray";
                } else {
                    ctx.fillStyle = this.map[line][column];
                }
                ctx.fillRect(column * (this.cellWidth + this.stroke) + this.stroke + this.mapX, line
                        * (this.cellWidth + this.stroke) + this.stroke + this.mapY, this.cellWidth - this.stroke,
                        this.cellWidth - this.stroke);
            }
        }
    }

    /**
     * Draw the falling stack on the map. The stack consists of a 4x4 matrix
     * containing '0' or '1', the value '1' means that the matrix cell is part
     * of the stack pattern. The stack is also associated with a color. This
     * function then consists to draw all the "filled" cells with the associated
     * color.
     */
    this.drawStack = function () {
        var canvas = document.getElementById("myCanvas");
        var ctx = canvas.getContext("2d");

        // Draw falling stack
        ctx.fillStyle = this.fallingStack.color;
        for (var line = 0; line < 4; line++) {
            for (var column = 0; column < 4; column++) {
                if (this.fallingStack.pattern[line][column] == 1) {
                    ctx.fillRect((column + this.fallingStack.x) * (this.cellWidth + this.stroke) + this.stroke
                            + this.mapX, (line + this.fallingStack.y) * (this.cellWidth + this.stroke) + this.stroke
                            + this.mapY, this.cellWidth - this.stroke, this.cellWidth - this.stroke);
                }
            }
        }
    }

    /**
     * Repaint the map (and the dropped stacks) and the falling stack.
     */
    this.repaint = function () {
        this.drawMap();
        this.drawStack();
    }

    /**
     * Drop the falling stack i.e. paste the stack pattern on the map grid, the
     * falling stack then became unmovable and the pattern cells will be see by
     * the map as unitary cells (not part of a stack anymore).
     */
    this.dropStack = function (stack) {
        for (var line = 0; line < 4; line++) {
            for (var column = 0; column < 4; column++) {
                if (stack.pattern[line][column] == 1) {
                    this.map[stack.y + line][stack.x + column] = stack.color;
                }
            }
        }
    }

    /**
     * This function is called when the user press 'up', 'down', 'left' or
     * 'right'. 'up' key is used to rotate the falling stack, 'down' key is used
     * to drop the stack or accelerate its fall, 'left' key is used to move the
     * stack to the left and 'right' key to the right.
     *
     * @param event
     *            The processed 'event' received from the web page event
     *            listener.
     */
    this.keyPressedHandler = function (event) {
        // Left key
        if (event.keyCode == 37) {
            // Move falling stack to the left
            if (this.fallingStack.x > 0) {
                this.fallingStack.x--;
            }
        }
        // Up key
        if (event.keyCode == 38) {
            // Rotate the falling stack
            // TODO
        }
        // Right key
        if (event.keyCode == 39) {
            // Move falling stack to the right
            if ((this.fallingStack.x + this.fallingStack.width) < this.mapWidth) {
                this.fallingStack.x++;
            }
        }
        // Down key
        if (event.keyCode == 40) {
            // Drop the stack in the air
            // TODO : or accelerate its fall
            while (this.isNextLineAvailable(this.fallingStack)) {
                this.fallingStack.y++
            }
        }

        // Repaint screen
        this.repaint();
    }

    /**
     * Returns 'true' if line below is available for the specified stack, or
     * 'false' otherwise.
     *
     * @param stack
     *            The specified stack.
     * @return 'true' if line below is available, 'false' otherwise.
     */
    this.isNextLineAvailable = function (stack) {
        // If floor reached
        if ((stack.y + stack.height) >= this.mapHeight) {
            return false;
        } else {
            // Check for each cell of the pattern, if next cell in map is
            // available
            for (var line = 0; line < 4; line++) {
                for (var column = 0; column < 4; column++) {
                    if (stack.pattern[line][column] == 1 && (this.map[stack.y + line + 1][stack.x + column] != null)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Return 'true' if stack is dropable, 'false' otherwise.
     *
     * @param stack
     *            The specified stack.
     * @return 'true' if stack is dropable, 'false' otherwise.
     */
    this.isStackDropable = function (stack) {
        for (var line = 0; line < 4; line++) {
            for (var column = 0; column < 4; column++) {
                if (stack.pattern[line][column] == 1 && (this.map[stack.y + line][stack.x + column] != null)) {
                    return false;
                }
            }
        }
        return true;
    }

}