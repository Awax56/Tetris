var map = new Map();    // Game map
var speed = 1000;       // Game speed in milliseconds
var timer = null;

/**
 * Main function of Tetris game.
 */
var main = function () {
    // Create map object
    map.initMap();
    map.drawMap();
    map.drawStack();

    // Add key event listener
    document.addEventListener("keydown", onKeyboardEvent, false);
}

/**
 * Start game loop.
 */
var startGameLoop = function () {
    timer = setInterval(gameLoop, speed);
}

/**
 * Stop game loop.
 */
var stopGameLoop = function () {
    clearInterval(timer);
    timer= null;
}

/**
 * Key handler processing keyboard events from user.
 */
var onKeyboardEvent = function (event) {
    map.keyPressedHandler(event);
}

/**
 * The game loop.
 */
var gameLoop = function () {
    // Compute gravity
    map.gravity();
}
